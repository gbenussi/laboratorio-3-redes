#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/un.h>
#include <netdb.h>
#include <stack>
#include <list>
#include <ctime>
#include <vector>
#include <cstring>
#include <iomanip>

#include "castings.cpp"

#define CARTA_A 0
#define CARTA_J 10
#define CARTA_Q 10
#define CARTA_K 10

#define ANCHO_CONSOLA 59

#define CLIENTE_1 0
#define CLIENTE_2 1

using namespace std;

// Puerto al que me conectare
int puerto=5009;
int TAMANO_BUFFER = 100;
int MAX = TAMANO_BUFFER - 1;
int pozo_total;
int partidas_ganadas[ 2 ];
int pozo_inicial = 100;
int apuesta_actual[ 2 ];
int pozo_clientes[ 2 ];
vector<int> mano_banca;

int cantidad_de_jugadores = 2;
bool jugadores_activos[ 2 ];
// Arreglo que indica que jugadores estan jugando
bool esta_jugando[ 2 ];

// Guardo una mano por cada cliente
vector< int > Manos[2];

// Vacia las manos de cada cliente
void VaciarManos(){
	for (int i = 0; i < cantidad_de_jugadores; ++i)
	{
		Manos[ i ].clear();
	}
	mano_banca.clear();
}


// Imprime las cartas de cada jugador
void VerManos(){
	cout << "Manos:" << endl;
	for (int i = 0; i < cantidad_de_jugadores; ++i)
	{
		cout << "\tJugador " << i << ": ";
		for (int k = 0; k < Manos[ i ].size(); ++k)
		{
			cout << VerCarta( Manos[ i ][ k ] ) << " ";
		}
		cout << endl;
	}
}

// Agrega la carta "carta" a las cartas del cliente "cliente"
void AgregarCarta(int cliente, int carta){
	Manos[ cliente ].push_back( carta );
}

// Retorna verdadero si hay jugadores activos (jugando)
bool HayJugadoresActivos(){
	for (int i = 0; i < cantidad_de_jugadores; ++i){
		if(esta_jugando[ i ])
			return true;
	}
	return false;
}

// Retorna verdadero si algun jugador esta pidiendo cartas
bool EstanPidiendoCartas(bool * esta_pidiendo_cartas){
	for (int i = 0; i < cantidad_de_jugadores; ++i)
	{
		if(esta_pidiendo_cartas[ i ])
			return true;
	}
	return false;
}

// Genera una carta entre 0 y 13
int GenerarCarta(){
	return rand() % 14;
}

// Retorna el id del cliente ganador o -1 en caso de empate
int ObtenerGanador(){
	int valor_mano[ 2 ];
	valor_mano[ 0 ] = ObtenerValorMano(Manos[ 0 ]);
	valor_mano[ 1 ] = ObtenerValorMano(Manos[ 1 ]);
	int valor_banca = ObtenerValorMano(mano_banca);
	if(valor_banca > 21)
		valor_banca = -1;
	// si el cliente 0 supero 21
	if(valor_mano[ 0 ] > 21){
		// si el cliente 1 supero 21, hay empate
		if(valor_mano[ 1 ] > 21)
			return -1;
		else{ // si el cliente 0 supero 21, y el cliente 1 no
			if(valor_banca > valor_mano[ 1 ])
				return -2;
			else
				return 1;
		}
	}
	// Lo mismo que el caso anterior
	if(valor_mano[ 1 ] > 21){
		if(valor_mano[ 0 ] > 21)
			return -1;
		else{
			if(valor_banca > valor_mano[ 0 ])
				return -2;
			else
				return 0;
		}
	}
	
	// Empate
	if(valor_mano[ 0 ] == valor_mano[ 1 ])
		return -1;

	// Gano el cliente 0
	if(valor_mano[ 0 ] > valor_mano[ 1 ]){
		if(valor_mano[ 0 ] >= valor_banca) // Gano el cliente 0 sobre la banca
			return 0;
		else
			return -2;
	}

	// Gano el cliente 1
	if(valor_mano[ 0 ] < valor_mano[ 1 ]){
		if(valor_banca >= valor_mano[ 1 ]) // Gano la banca
			return -2;
		else
			return 1;
	}
	// Empate
	return -1;
}

// Funcion que me muestra el estado de la partida
void VerDatos(){
	cout << endl;
	cout << " +----------+----------+----------+----------+--------+--------+" << endl;
	cout << " +----------+----------+    BLACKJACK RDC    +--------+--------+" << endl;
	cout << " +----------+----------+----------+----------+--------+--------+" << endl;
	cout << " |                                                             |" << endl;
	cout << " |        Pozo total: " << setw(28) << left << pozo_total << "             |" << endl;

	cout << " |        Mano Banca: ";
	int largo_total = 18;
	for (int k = 0; k < mano_banca.size(); ++k){
		cout << setw(2) << VerCarta(mano_banca[ k ]) << " ";
		largo_total += 3;
	}
	for (int l = 0; l < ANCHO_CONSOLA - largo_total; ++l)
	{
		cout << " ";
	}
	cout << "|" << endl;
	cout << " |        Puntaje   : " << setw(28) << left << ObtenerValorMano(mano_banca) << "             |" << endl;

	largo_total;
	for (int i = 0; i < cantidad_de_jugadores; ++i)
	{
		cout << " |                                                             |" << endl;
		cout << " |        Cliente " << i << ":                                           |" << endl;
		cout << " |             Pozo      : " <<  setw(4) << left << pozo_clientes[ i ] << "                                |" << endl;
		cout << " |             Apuesta   : " << setw(28) << left << apuesta_actual[ i ] << "        |" << endl;
		largo_total = 26;
		cout << " |                    Cartas: ";
		for (int k = 0; k < Manos[ i ].size(); ++k){
			cout << setw(2) << VerCarta(Manos[ i ][ k ]) << " ";
			largo_total += 3;
		}
		for (int l = 0; l < ANCHO_CONSOLA - largo_total; ++l)
		{
			cout << " ";
		}
		cout << "|" << endl;
		cout << " |                    Puntaje: " << setw(2) << left << ObtenerValorMano(Manos[i]) << "                              |" << endl;
	}
	cout << " |                                                             |" << endl;
	cout << " +----------+----------+----------+----------+--------+--------+" << endl;
}

int main(int argc,char **argv)
{
	cout << " +-----------+----------+----------+----------+--------+---------+" << endl;
	cout << " +-----------+----------+    BLACKJACK RDC    +--------+---------+" << endl;
	cout << " +-----------+----------+----------+----------+--------+---------+" << endl;
	cout << " | ___    _  ___    _   _  _   _  ___    _   _  _  ___    _____  |" << endl;
	cout << " | (  _`\\ (_)(  _`\\ ( ) ( )( ) ( )(  _`\\ ( ) ( )(_)(  _`\\ (  _  )|" << endl;
	cout << " | | (_) )| || (_(_)| `\\| || | | || (_(_)| `\\| || || | ) || ( ) ||" << endl;
	cout << " | |  _ <'| ||  _)_ | , ` || | | ||  _)_ | , ` || || | | )| | | ||" << endl;
	cout << " | | (_) )| || (_( )| |`\\ || \\_/ || (_( )| |`\\ || || |_) || (_) ||" << endl;
	cout << " | (____/'(_)(____/'(_) (_)`\\___/'(____/'(_) (_)(_)(____/'(_____)|" << endl;
	cout << " |                                                               |" << endl;
	cout << " +-----------+----------+----------+----------+--------+---------+" << endl;
	cout << endl;
	cout << "Esperando a los clientes . . ." << endl;
	/* ======================================= PREPARACION ======================================= */
	// Inicializo los datos
	for (int i = 0; i < cantidad_de_jugadores; ++i)
	{
		partidas_ganadas [ i ] = 0;
		esta_jugando     [ i ] = true;
		jugadores_activos[ i ] = true;
	}
	srand( time ( NULL ) );
	//SE ABRE EL DESCRIPTOR DEL SOCKET(SE CREA EL SOCKET)
	int Descriptor;
	Descriptor = socket(AF_INET, SOCK_STREAM, 0);
	if (Descriptor == -1)
	{
    		printf ("Error1\n");
	}


	//AHORA SE AVISA AL SISTEMA OPERATIVO QUE SE TIENE ABIERTO EL SOCKET Y QUE SE VA A ATENDER EL SERVICIO POR EL PUERTO DADO
	struct sockaddr_in Direccion;
	Direccion.sin_family = AF_INET;//TIPO DE CONEXION A ORDENADORES DE CUALQUIER TIPO
	Direccion.sin_port = htons(puerto);
	Direccion.sin_addr.s_addr =INADDR_ANY;//DIRECCION DEL CLIENTE,,,,SE ATIENDE A CUALQUIERA

	if ( bind(Descriptor, (struct sockaddr *)&Direccion, sizeof (Direccion)) == -1)
	{
		printf ("Error2\n");
	} 

	//AHORA SE DICE AL SISTEMA QUE COMIENCE A ATENDEER LAS LLAMADAS QUE LLEGUEN, CON EL DESCRIPTOR DEL SOCKET Y EL NUMERO MAXIMO DE CLIENTES A ENCOLAR
	if (listen(Descriptor, 1) == -1)
	{
		printf ("Error3\n");
	}



	//CON ESTO SE ESPERA Y ATIENDE A CUALQUIER CLIENTE QUE LLEGUE, CON LOS DATOS QUE SE SACAN DEL CLIENTE DE LA LLAMADA ACCEPT
	struct sockaddr_in Cliente;
	int Descriptor_Cliente[ 2 ];
	socklen_t Longitud_Cliente = sizeof(Cliente);

	Descriptor_Cliente[ CLIENTE_1 ] = accept(Descriptor, (struct sockaddr*)&Cliente, &Longitud_Cliente);
	printf("Se unio el Cliente 0 !\n");
	Descriptor_Cliente[ CLIENTE_2 ] = accept(Descriptor, (struct sockaddr*)&Cliente, &Longitud_Cliente);
	printf("Se unio el Cliente 1 !\n");
	if (Descriptor_Cliente[ CLIENTE_1 ] == -1)
	{
    		printf ("Error4.1\n");
	}
	if (Descriptor_Cliente[ CLIENTE_2 ] == -1)
	{
    		printf ("Error4.2\n");
	} 

	// Le envio a cada cliente su pozo inicial
	for (int i = 0; i < cantidad_de_jugadores; ++i)
	{
		write(Descriptor_Cliente[ i ], IntToString(pozo_inicial).c_str(), MAX);
		pozo_clientes[ i ] = pozo_inicial;
	}
	char buff[ TAMANO_BUFFER ];

	/* ======================================= APUESTA ======================================= */
	for (int i = 0; i < cantidad_de_jugadores; ++i)
	{
		// Le indico a cada cliente su id
		write(Descriptor_Cliente[ i ], IntToString(i).c_str(), MAX);
	}
	int carta;
	while(HayJugadoresActivos()){
		// Reseteo los atributos de la partida
		VaciarManos();
		cout << endl;
		cout << " +----------+----------+----------+----------+--------+--------+" << endl;
		cout << " +----------+----------+----------+----------+--------+--------+" << endl;
		cout << " +----------+                                         +--------+" << endl;
		cout << " +----------+               NUEVA PARTIDA             +--------+" << endl;
		cout << " +----------+                                         +--------+" << endl;
		cout << " +----------+----------+----------+----------+--------+--------+" << endl;
		cout << " +----------+----------+----------+----------+--------+--------+" << endl;
		// Reseteamos...
		pozo_total = 0;
		// Le digo a los clientes que me indiquen sus montos de apuesta
		//strcpy(buff,"Ingrese sus montos de apuesta: ");
		//write(Descriptor_Cliente[ CLIENTE_1 ], buff, MAX);
		//write(Descriptor_Cliente[ CLIENTE_2 ], buff, MAX);
		if(esta_jugando[ 0 ]){
			printf("Esperando a que el Cliente 1 agregue creditos al pozo. . .\n");
			read(Descriptor_Cliente[ CLIENTE_1 ], buff, MAX);
			pozo_total += StringToInt(buff);
			apuesta_actual[ 0 ] = StringToInt(buff);
			printf("El Cliente 1 agrego %d al pozo\n", StringToInt(buff));
		}else{
			apuesta_actual[ 0 ] = 0;
			
		}

		if(esta_jugando[ 1 ]){
			printf("Esperando a que el Cliente 2 agregue creditos al pozo. . .\n");
			read(Descriptor_Cliente[ CLIENTE_2 ], buff, MAX);
			pozo_total += StringToInt(buff);
			apuesta_actual[ 1 ] = StringToInt(buff);
			printf("El Cliente 2 agrego %d al pozo\n", StringToInt(buff));
		}else{
			apuesta_actual[ 1 ] = 0;
		}
		if(!esta_jugando[ 0 ] || !esta_jugando[ 1 ])
			pozo_total *= 2;
		

		pozo_clientes[ 0 ] -= apuesta_actual[ 0 ];
		pozo_clientes[ 1 ] -= apuesta_actual[ 1 ];


		carta = GenerarCarta();
		cout << "Agregando carta " << VerCarta(carta) << " a la banca" << endl;
		mano_banca.push_back(carta);
		carta = GenerarCarta();
		cout << "Agregando carta " << VerCarta(carta) << " a la banca" << endl;
		mano_banca.push_back(carta);

		/* ==================== INICIO JUEGO ==================== */

		// Genero una carta por cada jugador y se la envío
		for (int i = 0; i < cantidad_de_jugadores; ++i){
			if(esta_jugando[ i ]){
				for (int k = 0; k < 2; ++k)
				{
					carta = GenerarCarta();
					cout << "Enviando carta " << VerCarta(carta) << " al cliente " << i << endl;
					AgregarCarta(i, carta);
					write(Descriptor_Cliente[ i ], IntToString(carta).c_str(), MAX);
				}
			}
		}
		VerDatos();
		bool esta_pidiendo_cartas[ cantidad_de_jugadores ];
		for (int i = 0; i < cantidad_de_jugadores; ++i){
			if(esta_jugando[ i ])
				esta_pidiendo_cartas[ i ] = true;
			else
				esta_pidiendo_cartas[ i ] = false;
		}
		
		int valor_mano_0 = ObtenerValorMano(Manos[ 0 ]);
		int valor_mano_1 = ObtenerValorMano(Manos[ 1 ]);
		// Veo si hubo blackjack !
		if(valor_mano_0 == 21 || valor_mano_1 == 21){
			// si hubo blackjack le envio el id del ganador
			// si ambos tuvieron blackjack, envio -1
			if(valor_mano_0 == valor_mano_1){
				printf("Blackhjack de parte de ambos jugadores !\n");
				strcpy(buff, "4");
				write(Descriptor_Cliente[ 0 ], buff, MAX);
				write(Descriptor_Cliente[ 1 ], buff, MAX);
			}else{ // si solo un jugador hizo blackjack
				if(valor_mano_0 == 21){ // si fue el cliente 0
					printf("Blackhjack del cliente 0 !!!\n");
					strcpy(buff, "0");
					write(Descriptor_Cliente[ 0 ], buff, MAX);
					write(Descriptor_Cliente[ 1 ], buff, MAX);
					// Le envio el pozo que gano
					write(Descriptor_Cliente[ 0 ], IntToString(pozo_total).c_str(), MAX);
				}
				if(valor_mano_1 == 21){ // si fue el cliente 1
					printf("Blackhjack del cliente 1 !!!\n");
					strcpy(buff, "1");
					write(Descriptor_Cliente[ 0 ], buff, MAX);
					write(Descriptor_Cliente[ 1 ], buff, MAX);

					// Le envio el pozo que gano
					write(Descriptor_Cliente[ 1 ], IntToString(pozo_total).c_str(), MAX);
				}
			}
		}else{ // si no hubo blackjack
			int blackjack = -2;
			// Le aviso a los clientes que no hubo blackjack enviando un 3
			printf("No hubo blackjack\n");
			for (int i = 0; i < cantidad_de_jugadores; ++i){
				strcpy(buff, "3");
				if(esta_jugando[ i ])
					write(Descriptor_Cliente[ i ], buff, MAX);
			}
			
			// preguntara a los clientes (que esten en juego) si quieren mas cartas mientras
			// asi lo indiquen o hasta que la banca termine de pedir cartas
			while(EstanPidiendoCartas(esta_pidiendo_cartas) || ObtenerValorMano(mano_banca) < 16){
				// Cada cliente me enviara un 1 si quiere seguir recibiendo cartas
				// o un 0 si no lo quiere.
				for (int i = 0; i < cantidad_de_jugadores; ++i){
					if(esta_pidiendo_cartas[ i ] && esta_jugando[ i ]){
						printf("Esperando si el Jugador %d desea mas cartas . . .\n", i);
						memset(buff, 0, MAX + 1);
						read(Descriptor_Cliente[ i ], buff, MAX);
						if( StringToInt(buff) == 0){ // si el jugador no quiere recibir mas cartas
							esta_pidiendo_cartas[ i ] = 0;
							printf("El Jugador %d dejo de pedir cartas.\n", i);
						}else{ // si el jugador quiere mas cartas, le envio
							carta = GenerarCarta();
							write(Descriptor_Cliente[ i ], IntToString(carta).c_str(), MAX);
							AgregarCarta(i, carta);
							cout << "Enviando carta " << VerCarta(carta) << " al cliente " << i << endl;
							if(ObtenerValorMano(Manos[i]) >= 21)
								esta_pidiendo_cartas[i] = false;
						}
					}
				}
				// si la banca tiene 16 o menos puntos, debe pedir otra carta
				if(ObtenerValorMano(mano_banca) <= 16){
					carta = GenerarCarta();
					cout << "Agregando carta " << VerCarta(carta) << " a la banca" << endl;
					mano_banca.push_back(carta);
					VerDatos();
				}
			}
			// en este punto nadie quiere mas cartas
			printf("Nadie quiere mas cartas....\n");
			
			// Envio los resultados del ganador a cada jugador
			int ganador = ObtenerGanador();
			printf("Ganador: %d\n", ganador);

			// si el ganador fue un cliente
			if(ganador == 0 || ganador == 1){
				//printf("El Ganador es: %d, Gano: %d creditos !\n", ganador, pozo_total);
				pozo_clientes[ ganador ] += pozo_total;
			}
			else{
				//printf("No hay ganadores.\n");
			}
			ImprimirGanador(ganador);

			// le aviso quien fue el ganador a cada cliente que este jugando
			for (int i = 0; i < cantidad_de_jugadores; ++i)
			{
				strcpy(buff, IntToString(ganador).c_str());
				// Envio el ganador
				if(esta_jugando[ i ])
					write(Descriptor_Cliente[ i ], buff, MAX);
				// Envio el monto ganado a cada cliente
				strcpy(buff, IntToString(pozo_total).c_str());
				if(esta_jugando[ i ])
					write(Descriptor_Cliente[ i ], buff, MAX);
			}
		}
		for (int i = 0; i < cantidad_de_jugadores; ++i)
		{
			// si no esta en juego, indico que no esta jugando para que el servidor no le
			// pregunte si quiere mas cartas
			if(pozo_clientes[ i ] == 0)
				esta_jugando[ i ] = false;
		}
		printf("Esperando a ver que jugadores seguiran jugando . . .\n");

		// Recibo las respuestas de los jugadores
		for (int i = 0; i < cantidad_de_jugadores; ++i){
			if(esta_jugando[ i ]) // si el jugador i esta jugando, espero su respuesa
				read(Descriptor_Cliente[ i ], buff, MAX);
			else // si no, indico que no seguira jugando
				strcpy(buff, "0");
			printf("Jugador %d: ", i);
			// Asigno el estado del jugador al arreglo esta_jugando
			if(StringToInt(buff) == 0){
				printf("No\n");
				esta_jugando[ i ] = false;
			}else{
				printf("Si\n");
				esta_jugando[ i ] = true;
			}
		}
	}

	cout << endl;
	cout << " +----------+----------+----------+----------+--------+--------+" << endl;
	cout << " +----------+----------+    BLACKJACK RDC    +--------+--------+" << endl;
	cout << " +----------+----------+----------+----------+--------+--------+" << endl;
	cout << " |                        ___    _  _   _                      |" << endl;
	cout << " |                        (  _`\\ (_)( ) ( )                    |" << endl;
	cout << " |                        | (_(_)| || `\\| |                    |" << endl;
	cout << " |                        |  _)  | || , ` |                    |" << endl;
	cout << " |                        | |    | || |`\\ |                    |" << endl;
	cout << " |                        (_)    (_)(_) (_)                    |" << endl;
	cout << " |                                                             |" << endl;
	cout << " +----------+----------+----------+----------+--------+--------+" << endl;
	// Cierro los descriptores de los sockets
	close (Descriptor_Cliente[ CLIENTE_1 ]);
	close (Descriptor_Cliente[ CLIENTE_2 ]);
	close (Descriptor);
	return 0;
}
