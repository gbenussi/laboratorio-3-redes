#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/un.h>
#include <netdb.h>
#include <stack>
#include <list>
#include <ctime>
#include <vector>
#include <iomanip>

#include "castings.cpp"

#define ANCHO_CONSOLA 59

using namespace std;

int puerto= 5009;
int TAMANO_BUFFER = 100;
int MAX = TAMANO_BUFFER - 1;
int pozo_total;
int apuesta_actual;
vector< int > mano;
int id;

// Funcion que permite ver la mano de un jugador
void VerMano(){
	cout << "Mano: ";
	for (int i = 0; i < mano.size(); ++i)
	{
		cout << VerCarta( mano.at(i) ) << " ";
	}
	cout << endl;
}

// Funcion que imprime los datos mas relevantes de la partida actual
void VerDatos(){
	cout << endl;
	cout << " +----------+----------+----------+----------+--------+--------+" << endl;
	cout << " +----------+----------+    BLACKJACK RDC    +--------+--------+" << endl;
	cout << " +----------+----------+----------+----------+--------+--------+" << endl;
	cout << " |                                                             |" << endl;
	cout << " |             Pozo total: " << setw(28) << left << pozo_total << "        |" << endl;
	cout << " |             Apuesta   : " << setw(28) << left << apuesta_actual << "        |" << endl;
	int largo_total;
	largo_total = 26;
	cout << " |                    Cartas: ";
	for (int k = 0; k < mano.size(); ++k){
		cout << setw(2) << VerCarta(mano[ k ]) << " ";
		largo_total += 3;
	}
	for (int l = 0; l < ANCHO_CONSOLA - largo_total; ++l)
	{
		cout << " ";
	}
	cout << "|" << endl;
	cout << " |                    Puntaje: " << setw(2) << left << ObtenerValorMano(mano) << "                              |" << endl;
	cout << " |                                                             |" << endl;
	cout << " +----------+----------+----------+----------+--------+--------+" << endl;
}

int main(int argc,char **argv)
{
	cout << " +-----------+----------+----------+----------+--------+---------+" << endl;
	cout << " +-----------+----------+    BLACKJACK RDC    +--------+---------+" << endl;
	cout << " +-----------+----------+----------+----------+--------+---------+" << endl;
	cout << " | ___    _  ___    _   _  _   _  ___    _   _  _  ___    _____  |" << endl;
	cout << " | (  _`\\ (_)(  _`\\ ( ) ( )( ) ( )(  _`\\ ( ) ( )(_)(  _`\\ (  _  )|" << endl;
	cout << " | | (_) )| || (_(_)| `\\| || | | || (_(_)| `\\| || || | ) || ( ) ||" << endl;
	cout << " | |  _ <'| ||  _)_ | , ` || | | ||  _)_ | , ` || || | | )| | | ||" << endl;
	cout << " | | (_) )| || (_( )| |`\\ || \\_/ || (_( )| |`\\ || || |_) || (_) ||" << endl;
	cout << " | (____/'(_)(____/'(_) (_)`\\___/'(____/'(_) (_)(_)(____/'(_____)|" << endl;
	cout << " |                                                               |" << endl;
	cout << " +-----------+----------+----------+----------+--------+---------+" << endl;
	cout << endl;
	/* ===== PREPARACION ===== */

	// Descomentar la linea de abajo para usar siempre la misma ip
	//char ip[15] = "127.0.0.1";
	//SE CONSULTA POR DIRECCION IP DEL SERVIDOR
	printf("Ingrese la ip del servidor: \n");
	char ip[15];
	gets(ip);
	cout << "Conectando al servidor . . ." << endl;;


	//*********** SE DEFINE LA ESTRUCTURA DEL CLIENTE Y SE CONECTA AL SERVIDOR ************************
	int Descriptor;
	Descriptor = socket (AF_INET, SOCK_STREAM, 0);
	printf("Seteando puerto %d...", puerto);
	if (Descriptor == -1)
	{
    		printf ("Error.\n");
	}
	printf("Correcto.\n");
	
	struct sockaddr_in Direccion;
	Direccion.sin_family = AF_INET;
	Direccion.sin_addr.s_addr = inet_addr(ip);
	Direccion.sin_port = htons(puerto);

	printf("Conectando al servidor %s...", ip);
	if (connect(Descriptor, (struct sockaddr *)&Direccion, sizeof(Direccion) ) == -1)
	{
    		printf ("Error.\n");
	}
	printf("Correcto.\n");

	printf("Conexion Establecida !\n\n");
	
	// En este punto la conexion ya esta establecida

	/* ===== APUESTA ===== */
	

	// Creo el buffer que almacenara los mensajes recibidos
	char buff[ TAMANO_BUFFER ];

	// Recibo el pozo inicial que me indica el servidor
	printf("Esperando al servidor. . .\n");
	// Lo primero que me comunicara el servidor es el pozo inicial que tendre
	read(Descriptor, buff, MAX);
	pozo_total = StringToInt(buff);
	
	int seguir_jugando = 1;
	// El servidor me indica mi id
	read(Descriptor, buff, MAX);
	id = StringToInt(buff);
	cout << "Mi id es " << id << endl;
	do{
		mano.clear();
		cout << endl;
		cout << " +----------+----------+----------+----------+--------+--------+" << endl;
		cout << " +----------+----------+----------+----------+--------+--------+" << endl;
		cout << " +----------+                                         +--------+" << endl;
		cout << " +----------+               NUEVA PARTIDA             +--------+" << endl;
		cout << " +----------+                                         +--------+" << endl;
		cout << " +----------+----------+----------+----------+--------+--------+" << endl;
		cout << " +----------+----------+----------+----------+--------+--------+" << endl;
		// Le pido que ingrese su apuesta hasta que ingrese un valor:
		// 0 < valor <= pozo_total
		do{
			printf("Pozo Total: %d\n", pozo_total);
			printf("Ingrese su monto de apuesta: ");
			// Ingresos los montos de apuesta
			scanf("%s", buff);
			if(StringToInt(buff) > pozo_total || StringToInt(buff) <= 0)
				printf("Debe ingresar un valor mayor o igual a 0 o menor o igual a %d\n", pozo_total);
		}while(StringToInt(buff) > pozo_total || StringToInt(buff) <= 0);

		// descontamos la apuesta del pozo total
		apuesta_actual = StringToInt(buff);
		pozo_total -= apuesta_actual;

		// Le informo al servidor mi apuesta
		write(Descriptor, buff, MAX);

		printf("Esperando respuesta del servidor . . .\n");

		// El servidor me enviara dos cartas, asi que las espero
		read(Descriptor, buff, MAX);
		mano.push_back( StringToInt(buff) );
		cout << "Me llego la carta " << VerCarta(StringToInt(buff)) << " del servidor " << endl;
		read(Descriptor, buff, MAX);
		mano.push_back( StringToInt(buff) );
		cout << "Me llego la carta " << VerCarta(StringToInt(buff)) << " del servidor " << endl;
		VerDatos();

		// Veo si el servidor me aviso que hubo blackjack o no
		read(Descriptor, buff, MAX);
		int blackjack = StringToInt(buff);
		if(blackjack != 3){
			if(blackjack == 4){ // si ambos tuvieron blackjack
				ImprimirGanador(-1);
			}else if(blackjack == id){ // si yo gane
				cout << endl;
				cout << " +----------+----------+----------+----------+--------+-----------+" << endl;
				cout << " +----------+----------+    BLACKJACK RDC    +--------+-----------+" << endl;
				cout << " +----------+----------+----------+----------+--------+-----------+" << endl;
				cout << " |                                                                | " << endl;
				cout << " |(  _`\\ ( )    (  _  )(  _`\\ ( ) ( )(___  )(  _  )(  _`\\ ( ) ( ) | " << endl;
				cout << " || (_) )| |    | (_) || ( (_)| |/'/'    | || (_) || ( (_)| |/'/' | " << endl;
				cout << " ||  _ <'| |  _ |  _  || |  _ | , <   _  | ||  _  || |  _ | , <   | " << endl;
				cout << " || (_) )| |_( )| | | || (_( )| |\\`\\ ( )_| || | | || (_( )| |\\`\\  | " << endl;
				cout << " |(____/'(____/'(_) (_)(____/'(_) (_)`\\___/'(_) (_)(____/'(_) (_) | " << endl;
				cout << " |                                                                |" << endl;
				cout << " +------------+----------+----------+----------+--------+---------+" << endl;
				cout << endl;
				read(Descriptor, buff, MAX);
				printf("Agregue %d a mi pozo !\n", StringToInt(buff));
				pozo_total += StringToInt(buff);
			}else{
				ImprimirMiResultado(0);
			}
		}else{ // si no hubo blackjack
			if(ObtenerValorMano(mano) >= 21){
				write(Descriptor, "0", MAX);
			}else{
				do{
					printf("Desea otra carta: ");
					scanf("%d", &seguir_jugando);
					// Respondo al servidor
					write(Descriptor, IntToString(seguir_jugando).c_str(), MAX);
					if(seguir_jugando){
						printf("Esperando respuesta del servidor . . .\n");
						// Recibo la carta que me enviara el servidor
						read(Descriptor, buff, MAX);
						mano.push_back( StringToInt(buff) );
						VerDatos();
					}
				}while(seguir_jugando && ObtenerValorMano(mano) < 21);
			}
			VerMano();
			
			cout << "Suma mano: " << ObtenerValorMano(mano) << endl;

			printf("Deje de pedir cartas.\n");
			printf("Esperando a que los demas jugadores terminen.\n");
			// Espero a que el servidor me envie los resultados del juego
			// y luego le pregunto al usuario si desea seguir jugando

			// Espero a que el servidor me diga quien es el ganador
			read(Descriptor, buff, MAX);
			int ganador = StringToInt(buff);
			read(Descriptor, buff, MAX);
			int monto_ganado = StringToInt(buff);
			if(ganador == -1)
				ImprimirMiResultado(0);
			else{
				if(ganador == id)
					pozo_total += monto_ganado;

				cout << endl;
				if(ganador == id){
					ImprimirMiResultado(1);
					printf("Gane %d creditos.\n", monto_ganado);
				}
				else{
					ImprimirMiResultado(0);
				}
				cout << "Pozo: " << pozo_total << endl;
				cout << endl;
			}
		}


		printf("Desea seguir jugando? ");
		scanf("%d", &seguir_jugando);
		if(pozo_total > 0)
			write(Descriptor, IntToString(seguir_jugando).c_str(), MAX);
		else
			write(Descriptor, IntToString(0).c_str(), MAX);
	}while(seguir_jugando && pozo_total > 0);

	if(seguir_jugando && pozo_total <= 0){
		printf("Lo sentimos, no tiene creditos para seguir jugando.\n");
	}

	cout << endl;
	cout << " +----------+----------+----------+----------+--------+--------+" << endl;
	cout << " +----------+----------+    BLACKJACK RDC    +--------+--------+" << endl;
	cout << " +----------+----------+----------+----------+--------+--------+" << endl;
	cout << " |                        ___    _  _   _                      |" << endl;
	cout << " |                        (  _`\\ (_)( ) ( )                    |" << endl;
	cout << " |                        | (_(_)| || `\\| |                    |" << endl;
	cout << " |                        |  _)  | || , ` |                    |" << endl;
	cout << " |                        | |    | || |`\\ |                    |" << endl;
	cout << " |                        (_)    (_)(_) (_)                    |" << endl;
	cout << " |                                                             |" << endl;
	cout << " +----------+----------+----------+----------+--------+--------+" << endl;
	/*for(int i = 0; i < 5 ; i++)
	{
		memset(buff,0, MAX + 1);		
		printf("\nIngrese Mensaje %i: ", i + 1);
		fflush( stdout );
		gets(buff);
		write(Descriptor, buff, MAX);
	}*/
	close(Descriptor);
	return 0;
}

