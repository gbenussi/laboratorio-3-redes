#ifndef CONSTANTES
	#define CONSTANTES

#include <cstring>
#include <sstream>

using namespace std;

#define CLIENTE_1 0
#define CLIENTE_2 1

#define MENSAJE_SOLICITUD_POZO  1

int MAX = 100;
int MAX_IO = MAX - 1;

string IntToString(int numero){
	ostringstream ss;
    ss << numero;
	return ss.str();
}

int StringToInt(string mensaje){
	int numero    = 0;
	int inicio    = 0;
	bool negativo = false;

	if(mensaje[ 0 ] == '-'){
		inicio    = 1;
		negativo  = true;
	}

	for (int i = inicio; i < mensaje.length(); ++i)
	{
		numero *= 10;
		if(negativo)
			numero -= mensaje[ i ] - '0';
		else
			numero += mensaje[ i ] - '0';
	}
	return numero;
}

#endif