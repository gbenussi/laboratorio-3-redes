#include <cstring>
#include <sstream>

using namespace std;

#ifndef CASTINGS

// Transforma el numero "numero" en string
string IntToString(int numero){
	ostringstream ss;
    ss << numero;
	return ss.str();
}

// Transforma el string "mensaje" en int
int StringToInt(string mensaje){
	int numero    = 0;
	int inicio    = 0;
	bool negativo = false;

	if(mensaje[ 0 ] == '-'){
		inicio    = 1;
		negativo  = true;
	}

	for (int i = inicio; i < mensaje.length(); ++i)
	{
		numero *= 10;
		if(negativo)
			numero -= mensaje[ i ] - '0';
		else
			numero += mensaje[ i ] - '0';
	}
	return numero;
}

// Me devuelve la representacion "real" de una carta "carga".
// e.g.: Entrada: 11, Salida: J
string VerCarta(int carga){
	string salida = "";
	if(carga == 0)
		salida = "A";
	else if(carga < 10 && carga >= 0)
		salida = (char)(carga + '0');
	else if(carga == 10)
		salida = "10";
	else if(carga == 11)
		salida = "J";
	else if(carga == 12)
		salida = "Q";
	else if(carga == 13)
		salida = "K";
	else
		salida = "-";
	return salida;
}

// Me devuelve el valor que se le da en el juego a una carta
// e.g.: Entrada: J, Salida: 10
int ObtenerValorCarta(int carta){
	if(carta == 0)
		return 11;
	else if(carta > 10)
		return 10;
	else
		return carta;
}

// Me devuelve el valor de la mano "mano"
int ObtenerValorMano(vector< int > mano){
	int total = 0;
	for (int i = 0; i < mano.size(); ++i)
	{
		total += ObtenerValorCarta(mano.at(i));
	}
	return total;
}

// Imprime el estado que le indique
void ImprimirGanador(int id){
	cout << endl;
	cout << " +------------+----------+----------+----------+--------+-----------+" << endl;
	cout << " +------------+----------+    BLACKJACK RDC    +--------+-----------+" << endl;
	cout << " +------------+----------+----------+----------+--------+-----------+" << endl;
	if(id == 0){
		cout << " |    ___                             _                      __     |" << endl;
		cout << " |   (  _`\\                          ( )                   /' _`\\   |" << endl;
		cout << " |   | ( (_)   _ _   ___     _ _    _| |   _    _ __  _    | ( ) |  |" << endl;
		cout << " |   | |___  /'_` )/' _ `\\ /'_` ) /'_` | /'_`\\ ( '__)(_)   | | | |  |" << endl;
		cout << " |   | (_, )( (_| || ( ) |( (_| |( (_| |( (_) )| |    _    | (_) |  |" << endl;
		cout << " |   (____/'`\\__,_)(_) (_)`\\__,_)`\\__,_)`\\___/'(_)   (_)   `\\___/'  |" << endl;
		cout << " |                                                                  |" << endl;
	}else if   (id== 1){
		cout << " |    ___                             _                       _     |" << endl;
		cout << " |   (  _`\\                          ( )                    /' )    |" << endl;
		cout << " |   | ( (_)   _ _   ___     _ _    _| |   _    _ __  _    (_, |    |" << endl;
		cout << " |   | |___  /'_` )/' _ `\\ /'_` ) /'_` | /'_`\\ ( '__)(_)     | |    |" << endl;
		cout << " |   | (_, )( (_| || ( ) |( (_| |( (_| |( (_) )| |    _      | |    |" << endl;
		cout << " |   (____/'`\\__,_)(_) (_)`\\__,_)`\\__,_)`\\___/'(_)   (_)     (_)    |" << endl;
	}else if(id == -1){
		cout << " |            ___                             _                     |" << endl;
		cout << " |           (  _`\\                          ( )_                   |" << endl;
		cout << " |           | (_(_)  ___ ___   _ _      _ _ | ,_)   __             |" << endl;
		cout << " |           |  _)_ /' _ ` _ `\\( '_`\\  /'_` )| |   /'__`\\           |" << endl;
		cout << " |           | (_( )| ( ) ( ) || (_) )( (_| || |_ (  ___/           |" << endl;
		cout << " |           (____/'(_) (_) (_)| ,__/'`\\__,_)`\\__)`\\____)           |" << endl;
		cout << " |                             | |                                  |" << endl;
		cout << " |                             (_)                                  |" << endl;
	}else{
		cout << " |  ___                            ___                              |" << endl;
		cout << " | (  _`\\                         (  _`\\                            |" << endl;
		cout << " | | ( (_)   _ _   ___     _      | (_) )   _ _   ___     ___    _ _|" << endl;
		cout << " | | |___  /'_` )/' _ `\\ /'_`\\    |  _ <' /'_` )/' _ `\\ /'___) /'_` |" << endl;
		cout << " | | (_, )( (_| || ( ) |( (_) )   | (_) )( (_| || ( ) |( (___ ( (_| |" << endl;
		cout << " | (____/'`\\__,_)(_) (_)`\\___/'   (____/'`\\__,_)(_) (_)`\\____)`\\__,_|" << endl;
	}
	cout << " |                                                                  |" << endl;
	cout << " +------------+----------+----------+----------+--------+-----------+" << endl;
	cout << endl;
}

// Imprime ganador o perdedor dependiendo de "resultado"
// resultado == 1 -> Ganador
// resultado == 0 -> Perdedor
void ImprimirMiResultado(int resultado){
	cout << endl;
	cout << " +------------+----------+----------+----------+--------+-----------+" << endl;
	cout << " +------------+----------+    BLACKJACK RDC    +--------+-----------+" << endl;
	cout << " +------------+----------+----------+----------+--------+-----------+" << endl;
	if(resultado == 1){
		cout << " |         ___                             _                        |" << endl;
		cout << " |        (  _`\\                          ( )                       |" << endl;
		cout << " |        | ( (_)   _ _   ___     _ _    _| |   _    _ __           |" << endl;
		cout << " |        | |___  /'_` )/' _ `\\ /'_` ) /'_` | /'_`\\ ( '__)          |" << endl;
		cout << " |        | (_, )( (_| || ( ) |( (_| |( (_| |( (_) )| |             |" << endl;
		cout << " |        (____/'`\\__,_)(_) (_)`\\__,_)`\\__,_)`\\___/'(_)             |" << endl;
	}else{
		cout << " |         ___                   _             _                    |" << endl;
		cout << " |        (  _`\\                ( )           ( )                   |" << endl;
		cout << " |        | |_) )  __   _ __   _| |   __     _| |   _    _ __       |" << endl;
		cout << " |        | ,__/'/'__`\\( '__)/'_` | /'__`\\ /'_` | /'_`\\ ( '__)      |" << endl;
		cout << " |        | |   (  ___/| |  ( (_| |(  ___/( (_| |( (_) )| |         |" << endl;
		cout << " |        (_)   `\\____)(_)  `\\__,_)`\\____)`\\__,_)`\\___/'(_)         |" << endl;
	}
	cout << " |                                                                  |" << endl;
	cout << " +------------+----------+----------+----------+--------+-----------+" << endl;
	cout << endl;
}

#endif